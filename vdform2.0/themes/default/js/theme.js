﻿//错误后的提示层
var onErrorHtml = '<div class="validator_msg_tip"><div class="tip-error-ico"></div><div class="validator_tip_msg_content">$data$</div><div class="validator_msg_tip_direction validator_msg_tip_left"><em>◆</em><span>◆</span></div></div>';
//成功后的提示层
var onSuccessHtml = '<div class="validator_msg_tip"><div class="tip-success-ico"></div><div class="validator_tip_msg_content">$data$</div><div class="validator_msg_tip_direction validator_msg_tip_left"><em>◆</em><span>◆</span></div></div>';
//错误后的input样式
var onErrorInputClass='validato_input';
//谷歌下的offset
var offsetChromeLeft = 0;
var offsetChromeTop = 15; 
//IE下的offset   
var offsetIELeft = 0;
var offsetIETop = 15;